unit ll_interfaces;

{$mode objfpc}

interface

uses
  Classes, SysUtils, Controls;

type

  { ILabelsOwner }

  // интерфейс для владельца меток
  ILabelsOwner = interface
    ['{C465B1F4-EDA5-464C-924A-EE4EB98D8389}']
    // делает себя владельцем метки
    procedure SetParentForm(l: TGraphicControl);
    // смещение для группы снизу
    function GetTopOffset: integer;
    // смещение для группы слева
    function GetLeftOffset: integer;
    // процедура для обновления окна
    procedure Refresh;
  end;

  { ILabelsList }

  // интерфейс контейнера меток
  ILabelsContainer = interface
    ['{AD4E7FBB-53BA-4BC3-ABF8-232D9F0F9A41}']
    // добавляет метку с текстом в строке capt
    procedure AddLabel(capt: string);
  end;

implementation
end.

