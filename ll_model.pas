unit ll_model;

{$mode objfpc}

interface

uses
  Classes, SysUtils, StdCtrls, ll_interfaces, fgl;

type
  // для удобства и тренировки работы с дженериками
  TLabelListGenerics = specialize TFPGObjectList<TCustomLabel>;

  { TLabelsList }

  TLabelsList = class(TLabelListGenerics)
  public
    procedure AddNew(const ACaption: string; AOwner: ILabelsOwner);
    procedure CheckMaxCount(AMaxCount: Integer);
    procedure Update(ALeft, ATop: Integer);
  end;


   { TLabelsContainer }

  TLabelsContainer = class(TInterfacedObject, ILabelsContainer)
  private
    fMaxLabels: smallint;             // максимум меток
    fLabels: TLabelsList;             // список меток
    LabelsOwner: ILabelsOwner;        // владелец меток
  public
    procedure AddLabel(capt: string);
    procedure UpdateLabels;
    constructor Create(max: smallint; owner: ILabelsOwner);
    destructor Destroy; override;
  end;

implementation

{ TLabelsList }

procedure TLabelsList.AddNew(const ACaption: string; AOwner: ILabelsOwner);
var
  LAddedLabel: TCustomLabel;
begin
  LAddedLabel := TCustomLabel.Create(nil);
  LAddedLabel.Caption := ACaption;
  LAddedLabel.Font.Color:=$936868;        // приятный цвет
  Insert(0, LAddedLabel);
  AOwner.SetParentForm(LAddedLabel);
end;

procedure TLabelsList.CheckMaxCount(AMaxCount: Integer);
begin
  if Count = AMaxCount then
    Delete(Count - 1);     // освободит память сама
end;

procedure TLabelsList.Update(ALeft, ATop: Integer);
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    begin
      Items[i].Left := ALeft;
      Items[i].Top := ATop - (i * 16);
    end;
end;

{ TLabelsList }

procedure TLabelsContainer.AddLabel(capt: string);
begin
     // в принципе можно было реализовать и не списком, а очередью :)
  fLabels.CheckMaxCount(fMaxLabels);
  fLabels.AddNew(capt, LabelsOwner);
  UpdateLabels;
end;

procedure TLabelsContainer.UpdateLabels;
begin
  fLabels.Update(LabelsOwner.GetLeftOffset, LabelsOwner.GetTopOffset);
  LabelsOwner.Refresh;
end;

constructor TLabelsContainer.Create(max: smallint; owner: ILabelsOwner);
begin
  inherited Create;
  fLabels := TLabelsList.Create;
  fMaxLabels := max;
  LabelsOwner := owner;
end;

destructor TLabelsContainer.Destroy;
begin
  fLabels.Clear;
  fLabels.Free;
  inherited Destroy;
end;

end.

